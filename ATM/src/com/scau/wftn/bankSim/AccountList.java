package com.scau.wftn.bankSim;

import java.util.HashMap;
import java.util.Map;

import com.scau.wftn.ATM.Card;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class AccountList {
	Map<String,Account> map = new HashMap<String, Account>();
	public AccountList(){
		Client client = new Client("傻逼", "4451212564454544", "18819261048");
		Client client1 = new Client("傻逼1", "4451212564454543", "18819261047");
		Account account1 = new Account(client, "123", new Cash(111,"RMB"),new Card("595959595959"));
		Account account2 = new Account(client1, "1234", new Cash(123,"RMB"),new Card("45454546546"));
		map.put("595959595959", account1);
		map.put("45454546546", account2);
	}
	
	//根据卡号获得帐号
	public Account getAccountByCard(Card card){
		return map.get(card.getCardNo());
	}
}
