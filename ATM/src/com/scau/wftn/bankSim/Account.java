package com.scau.wftn.bankSim;

import java.util.List;

import com.scau.wftn.ATM.Card;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
@SuppressWarnings("all")
public class Account {
	private Card card;
	private Client client;
	private String password;
	private Cash balance;
	private List<UserLog> logs;
	
	public Account(Client client,String password,Cash balance,Card card){
		this.balance = balance;
		this.client = client;
		this.password = password;
		this.card = card;
	}
	

	public Card getCard() {
		return card;
	}


	public void setCard(Card card) {
		this.card = card;
	}


	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<UserLog> getLogs() {
		return logs;
	}
	public void setLogs(List<UserLog> logs) {
		this.logs = logs;
	}
	public Cash getBalance() {
		return balance;
	}

	public void setBalance(Cash balance) {
		this.balance = balance;
	}
	
}
