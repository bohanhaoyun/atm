package com.scau.wftn.bankSim;

import java.util.Date;

import com.scau.wftn.ATM.Card;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class AccountServiceSim {
	private Account account;
	private AccountList accountList = new AccountList();
	
	//验证
	public boolean authorize(Card card,String password){
		account = accountList.getAccountByCard(card);
		if(account.getPassword().equals(password)){
			addUserLog(new UserLog(card,"登录",null,new Date()));
			return true;
		}else{
			return false;			
		}
	}
	
	//取款
	public String withdrawl(Cash cash,Card card){
		account = accountList.getAccountByCard(card);
		if(cash.getAmount()<=account.getBalance().getAmount()){
			System.out.println("取款前余额："+account.getBalance().getAmount());
			account.getBalance().setAmount(account.getBalance().getAmount()-cash.getAmount());
			System.out.println("取款后余额："+account.getBalance().getAmount());
			addUserLog(new UserLog(account.getCard(),"取款",cash,new Date()));
//			addUserLog(UserLog);
			return "取款成功";
		}else{
			return "账户余额不足";
		}
	}
	
	//添加日志
	public void addUserLog(UserLog userLog){
		System.out.println("添加日志成功");
	}
}
