package com.scau.wftn.bankSim;

import java.math.BigInteger;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class Client {
	private String name;
	private String identifyId;
	private String telephone;
	
	public Client(String name,String identifyId,String telephone){
		this.name = name;
		this.identifyId = identifyId;
		this.telephone = telephone;
	}
}
