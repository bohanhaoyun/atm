package com.scau.wftn.bankSim;

import java.util.Date;

import com.scau.wftn.ATM.Card;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class UserLog {

	private Card card;
	private String operation;
	private Cash cash;
	private Date date;
	
	public UserLog(Card card,String operation,Cash cash,Date date){
		this.card = card;
		this.cash = cash;
		this.operation = operation;
		this.date = date;
	}
	
	public Card getCard() {
		return card;
	}
	public void setCard(Card card) {
		this.card = card;
	}
	public Cash getCash() {
		return cash;
	}
	public void setCash(Cash cash) {
		this.cash = cash;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
}
