package com.scau.wftn.util;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class Cash {
	private int amount;
	private String currency;
	
	public Cash(int amount,String currency){
		this.amount = amount;
		this.currency = currency;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
