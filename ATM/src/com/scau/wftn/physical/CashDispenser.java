package com.scau.wftn.physical;

import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class CashDispenser {
	Cash ATMcash = new Cash(9999,"RMB"); 
	
	//验证金额
	public boolean verifyCash(int amount){
		if(ATMcash.getAmount()>=amount){
			return true;
		}else{
			return false;
		}
	}
	
	//吐钞
	public void spitCash(int amount){
			System.out.println("吐钞前ATM金额：" + ATMcash.getAmount());
			ATMcash.setAmount(ATMcash.getAmount()-amount);
			System.out.println("吐钞成功");
			System.out.println("吐钞后ATM金额：" + ATMcash.getAmount());
	}

	public Cash getATMcash() {
		return ATMcash;
	}

	public void setATMcash(Cash aTMcash) {
		ATMcash = aTMcash;
	}
	
	
}
