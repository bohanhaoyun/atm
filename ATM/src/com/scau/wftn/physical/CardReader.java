package com.scau.wftn.physical;

import com.scau.wftn.ATM.Card;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class CardReader {
	private Card card;
	
	
	//读卡
	public void readCard(String cardNo){
		card = new Card(cardNo);
	}
	
	
	public Card getCard() {
		return card;
	}
	public void setCard(Card card) {
		this.card = card;
	}
	
}
