package com.scau.wftn.physical;

import java.util.Date;

import com.scau.wftn.ATM.Card;
import com.scau.wftn.ATM.Receipt;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class ReceiptPrinter {
	private Receipt receipt;
	public void print(Card card,String transType,Cash amount){
		receipt = new Receipt();
		receipt.setAmount(amount);
		receipt.setCard(card);
		receipt.setTransType(transType);
		receipt.setDate(new Date());
		System.out.println(receipt.toString());
	}
}
