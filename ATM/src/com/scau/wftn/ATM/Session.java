package com.scau.wftn.ATM;

import java.util.Date;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class Session {
	private int lifecycle = 30; //生命周期
	private Card card;
	private Date date;
	
	public Session(Card card){
		this.card = card;
	}

	public int getLifecycle() {
		return lifecycle;
	}

	public void setLifecycle(int lifecycle) {
		this.lifecycle = lifecycle;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
