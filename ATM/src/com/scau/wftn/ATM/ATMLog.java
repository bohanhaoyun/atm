package com.scau.wftn.ATM;

import java.util.Date;

import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class ATMLog {

	private Card card;
	private String type;
	private Cash cash;
	private Date date;
	
	public ATMLog(Card card,String type, Cash cash, Date date){
		this.card = card;
		this.cash = cash;
		this.date = date;
		this.type = type;
	}
	
	public Card getCard() {
		return card;
	}
	public void setCard(Card card) {
		this.card = card;
	}
	public Cash getCash() {
		return cash;
	}
	public void setCash(Cash cash) {
		this.cash = cash;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
