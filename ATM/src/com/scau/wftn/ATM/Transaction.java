package com.scau.wftn.ATM;

import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public interface Transaction {
	//处理交易
	public String processTransction(Card card);
	//获取交易类型
	public String getTransType();
	//获取交易金额
	public Cash getCash();
}
