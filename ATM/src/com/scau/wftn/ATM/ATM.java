package com.scau.wftn.ATM;

import java.util.Date;

import com.scau.wftn.bankSim.AccountServiceSim;
import com.scau.wftn.bankSim.UserLog;
import com.scau.wftn.physical.CardReader;
import com.scau.wftn.physical.CashDispenser;
import com.scau.wftn.physical.ReceiptPrinter;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class ATM {
	private CardReader cardReader = new CardReader();
	private CashDispenser cashDispenser = new CashDispenser();
	private ReceiptPrinter receiptPrinter = new ReceiptPrinter();
	private AccountServiceSim accountService = new AccountServiceSim();
	private Transaction transaction;
	private Session session;
	//插卡
	public void plugin(String cardNo){
		cardReader.readCard(cardNo);
	}
	
	//验证操作
	public void authorize(String password){
		if(accountService.authorize(cardReader.getCard(), password)){
			session = new Session(cardReader.getCard());
		}else{
			System.out.println("输入密码错误");
		}
	}
	//取款
	public void withdrawl(int amount){
		transaction = new Withdrawl(new Cash(amount,"RMB"));
		if(cashDispenser.verifyCash(amount)){
			//远程交易
			System.out.println(transaction.processTransction(session.getCard()));
			
			//本地操作
			cashDispenser.spitCash(amount);
			addATMLog(new ATMLog(session.getCard(),"减少", new Cash(amount,"RMB"), new Date()));
//			addATMLog(ATMLog)session.getCard(),"减少", new Cash(amount,"RMB"), new Date()));
		}else{
			System.out.println("本机现金不足");
		}
		
	}
	
	public void print(boolean isPrint){
		if(isPrint){
			receiptPrinter.print(cardReader.getCard(), transaction.getTransType(), transaction.getCash());			
			transaction = null;
			System.out.println("交易结束");
		}else{
			transaction = null;
			System.out.println("交易结束");
		}
		
	}
	
	//添加日志
		public void addATMLog(ATMLog atmLog){
			System.out.println("添加日志成功");
		}
	
	public void endSession(){
		session = null;
		System.out.println("退卡");
	}
	
	public CardReader getCardReader() {
		return cardReader;
	}
	public void setCardReader(CardReader cardReader) {
		this.cardReader = cardReader;
	}
	public CashDispenser getCashDispenser() {
		return cashDispenser;
	}
	public void setCashDispenser(CashDispenser cashDispenser) {
		this.cashDispenser = cashDispenser;
	}
	public ReceiptPrinter getReceiptPrinter() {
		return receiptPrinter;
	}
	public void setReceiptPrinter(ReceiptPrinter receiptPrinter) {
		this.receiptPrinter = receiptPrinter;
	}
}

