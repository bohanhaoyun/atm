package com.scau.wftn.ATM;

import java.util.Date;

import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class Receipt {
	private Card card;
	private String transType;
	private Cash amount;
	private Date date;
	public Card getCard() {
		return card;
	}
	public void setCard(Card card) {
		this.card = card;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public Cash getAmount() {
		return amount;
	}
	public void setAmount(Cash amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Receipt [card=" + card + ", transType=" + transType
				+ ", amount=" + amount + ", date=" + date + "]";
	}
	
}
