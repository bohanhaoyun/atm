package com.scau.wftn.ATM;

/**
 * @author bohan
 * @create 2017年5月10日
 */
public class Card {
	private String cardNo;
	
	public Card(String cardNo){
		this.cardNo = cardNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
}
