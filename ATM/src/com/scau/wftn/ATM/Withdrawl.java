package com.scau.wftn.ATM;

import com.scau.wftn.bankSim.AccountServiceSim;
import com.scau.wftn.util.Cash;

/**
 * @author bohan
 * @create 2017年5月11日
 */
public class Withdrawl implements Transaction{
	private String transType = "取款";
	private Cash cash;
	AccountServiceSim service = new AccountServiceSim();
	public Withdrawl(Cash cash){
		this.cash = cash;
	}
	
	public String processTransction(Card card) {
		return service.withdrawl(cash, card);
	}

	public String getTransType() {
		return transType;
	}

	public Cash getCash() {
		return cash;
	}

}
